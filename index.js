var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('msg', function(data){
    console.log(data);
    io.emit('msg', data);
  });
});

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});



